import React, { Component } from "react";
import { Icon } from "react-native-elements";
import { createStackNavigator, createAppContainer } from "react-navigation";
import { StyleSheet, TouchableOpacity, Text, View, Button } from "react-native";

export default class Project extends React.Component {
  constructor() {
    super();

    this.state = {
      show: false
    };
  }
  //   addCommand = () => {
  //     this.props.navigation.navigate("Login");
  // }

  ShowHideComponent = () => {
    if (this.state.show == true) {
      this.setState({ show: false });
    } else {
      this.setState({ show: true });
    }
  };

  render() {
    return (
      <View>
        <TouchableOpacity
          style={styles.container}
          onPress={this.ShowHideComponent}
        >
          <View style={styles.title}>
            <Icon name="g-translate" color="#00aced" />
            <Text style={{ marginLeft: 4 }}>
              {this.props.project.projectName}
            </Text>
          </View>
          <Text style={styles.text}>{this.props.project.projectStatus}</Text>
        </TouchableOpacity>

        <View>
          {/*Here we will return the view when state is true 
    and will return false if state is false*/}
          {this.state.show ? (
            <View style={styles.projectDetail}>
              <Text>ProjectName : {this.props.project.projectDescription}</Text>
              <Text>ProjectOwner :{this.props.project.projectOwner}</Text>
              <Text>Start Date : {this.props.project.startDate}</Text>
              <Text>End Date : {this.props.project.endDate}</Text>
              <Text>Project Status : {this.props.project.projectStatus}</Text>
              <Button style={styles.button}
                title="Update"
                onPress={() =>
                  this.props.navigation.navigate("ProjectDetail", {
                    project: this.props.project
                  })
                }
              />
            </View>
          ) : null}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: "flex-start",
    justifyContent: "space-between",
    padding: 16,
    borderRadius: 4,

    borderWidth: 1,
    marginTop: 20
  },
  text: {
    color: "#B2BEB5",
    fontWeight: "bold"
  },
  title: {
    paddingVertical: 15,

    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  projectDetail: {
    marginTop: 10,
    marginLeft: 10
  },
  button: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgb(71, 141, 226)",
    fontSize: 40,
    fontWeight: "bold",
    borderRadius: 10,
    height: 50,
    padding: 10
  }
});
