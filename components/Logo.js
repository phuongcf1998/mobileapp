import * as React from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';

export default class AssetExample extends React.Component {
  render() {
    return (
      <View style={styles.container}>
     
         <Image style={styles.logo} source={require('../assets/00f0c817-375b-4710-a67a-dc78eba9e390_200x200.png')} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
   
  },
  
  logo: {
    height: 200,
    width: 200,
  }
});
