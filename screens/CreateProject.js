import React, { Component } from "react";
import {
  StyleSheet,
  TextInput,
  View,
  Button,
  Text,
  KeyboardAvoidingView,
  Keyboard,
  TouchableOpacity,
  ScrollView,
  Alert
} from "react-native";
import { Icon } from "react-native-elements";
import axios from "axios";
import { createStackNavigator, createAppContainer } from "react-navigation";
import DatePicker from "../components/DatePicker";
export default class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      UserName: "",
      FullName: "",
      Email: "",
      Password: ""
    };
  }
  static navigationOptions = {
    title: "Register"
  };

  submitForm = () => {
    var self = this;
    if (this.state.Password.length < 6) {
      Alert.alert("Error", "Password must > 6 characters", [
        {
          text: "Okay"
        }
      ]);
    } else if (this.state.Email.indexOf("@") === -1) {
      Alert.alert("Message", "Email not correct", [
        {
          text: "Okay"
        }
      ]);
    } else {
        self.moveToLoginScreen();

    //   axios
    //     .post("http://34.67.239.2/phuonglhk_app/public/api/register", {
    //       userId: this.state.UserName,
    //       fullName: this.state.FullName,
    //       email: this.state.Email,
    //       password: this.state.Password,
    //       isActived: 1
    //     })
    //     .then(function(response) {
    //       // handle success
    //       self.moveToLoginScreen.bind(response);
         
    //       //   Alert.alert("Register successfully", [
    //       //     {
    //       //       text: "Okay"
    //       //     }
    //       //   ]);
    //     })
    //     .catch(function(error) {
    //       Alert.alert("Error", "Email or password not correct", [
    //         {
    //           text: "Okay"
    //         }
    //       ]);
    //     });
    }
  };
  moveToLoginScreen = () => {
    Alert.alert(
      //title
      "Message",
      //body
      "Register successfully",
      [
        { text: "Ok", onPress: () =>  this.props.navigation.navigate("Login") },
        
      ],
      { cancelable: true }
    );
  };
  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: "#FFFFFF",
          justifyContent: "center"
        }}
      >
        <ScrollView keyboardShouldPersistTaps="handled">
          <View style={{ marginTop: 100 }}>
            <KeyboardAvoidingView enabled>
              <View style={styles.SectionStyle}>
                <Icon
                  name="user"
                  type="font-awesome"
                  size={22}
                  iconStyle={{ padding: 10 }}
                  color="#413E4F"
                />

                <DatePicker/>
              </View>
              <View style={styles.SectionStyle}>
                <Icon
                  name="id-card"
                  type="font-awesome"
                  size={16}
                  iconStyle={{ padding: 10 }}
                  color="#413E4F"
                />

                <TextInput
                  style={{ flex: 1, color: "#413E4F" }}
                  onChangeText={FullName => this.setState({ FullName })}
                  underlineColorAndroid="#413E4F"
                  placeholder="Enter Full Name"
                  placeholderTextColor="#413E4F"
                  autoCapitalize="sentences"
                />
              </View>
              <View style={styles.SectionStyle}>
                <Icon
                  name="envelope"
                  type="font-awesome"
                  size={18}
                  iconStyle={{ padding: 10 }}
                  color="#413E4F"
                />

                <TextInput
                  style={{ flex: 1, color: "#413E4F" }}
                  onChangeText={Email => this.setState({ Email })}
                  underlineColorAndroid="#413E4F"
                  placeholder="Enter  Email"
                  placeholderTextColor="#413E4F"
                  autoCapitalize="sentences"
                  keyboardType="email-address"
                />
              </View>
              <View style={styles.SectionStyle}>
                <Icon
                  name="lock"
                  type="font-awesome"
                  size={24}
                  iconStyle={{ padding: 12 }}
                  color="#413E4F"
                />

                <TextInput
                  style={{ flex: 1, color: "#413E4F" }}
                  onChangeText={Password => this.setState({ Password })}
                  underlineColorAndroid="#413E4F"
                  placeholder="Enter Password"
                  placeholderTextColor="#413E4F"
                  secureTextEntry="true"
                />
              </View>

              <TouchableOpacity
                style={styles.ButtonStyle}
                activeOpacity={0.5}
                onPress={_ => this.submitForm()}
              >
                <Text
                  style={{
                    color: "#FFFFFF",
                    paddingVertical: 10
                  }}
                >
                  REGISTER
                </Text>
              </TouchableOpacity>
            </KeyboardAvoidingView>
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  SectionStyle: {
    flexDirection: "row",
    height: 40,
    marginTop: 20,
    marginLeft: 35,
    marginRight: 35,
    margin: 10
  },

  ButtonStyle: {
    backgroundColor: "rgb(71, 141, 226)",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#51D8C7",
    height: 40,
    alignItems: "center",
    borderRadius: 5,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 30
  }
});
