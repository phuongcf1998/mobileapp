import * as React from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Alert,
  KeyboardAvoidingView , Platform
} from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import axios from "axios";
// You can import from local files
import Logo from "../components/Logo";

import FormTextInput from "../components/FormTextInput";

export default class Login extends React.Component {
  state = { email: "", password: "" };

  checkLogin() {
    const { email, password } = this.state;
    // if(email=='huynh1@fpt.edu.vn'&&password=='Snowfox1991'){
    //   this.props.navigation.navigate('DashBoard')
    // }else{
    //   Alert.alert('Error','Email or password not correct',[{
    //     text:'Okay'
    //   }])
    // }
    var self = this;
    axios
      .post("http://34.67.239.2/phuonglhk_app/public/api/login", {
        email: email,
        password: password
      })
      .then(function(response) {
        // handle success
        self.props.navigation.navigate("DashBoard", { token: response.data });
      })
      .catch(function(error) {
        Alert.alert("Error", "Email or password not correct", [
          {
            text: "Okay"
          }
        ]);
      });
    // await axios.get("https://jsonplaceholder.typicode.com/todos/1")
    //   .then(function(response) {
    //     console.warn(response.data);
    //   }).catch(function (error) {
    //     console.warn(error);
    //   })
  }

  render() {
    return (
      <View style={styles.container}
     >
        <Logo />

        <TextInput
          style={styles.input}
          placeholder="Email"
          onChangeText={text => this.setState({ email: text })} 
          value={this.state.text}
        />

        <TextInput
          style={styles.input}
          secureTextEntry={true}
          placeholder="password"
          onChangeText={text => this.setState({ password: text })}
          value={this.state.text}
        />

        <TouchableOpacity
          style={styles.button}
          onPress={_ => this.checkLogin()}
        >
          <Text style={styles.text}>Login</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate('Register')}>
          <Text style={styles.text}>Register</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    
    flex: 1,
    alignItems: "center",
    
  },
  // containerInput: {
  //   flex: 1,
  //   justifyContent: "center",
  //   paddingHorizontal: 30,
  //   margin: 40
  // },

  input: {
    height: 40,
    backgroundColor: "#ffffff",
    width: 300,
    height: 44,
    padding: 10,
    borderRadius: 10,
    borderWidth: 0.5,
    borderColor: "#d6d7da",
    marginBottom: 20,
  },
  // containerButton: {
  //   flex: 1,
  //   justifyContent: "center",
  //   paddingHorizontal: 50,
  //   marginTop: 70
  // },
  button: {
    width: "70%",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgb(71, 141, 226)",
    fontSize: 40,
    fontWeight: "bold",
    borderRadius: 10,
    height: 50,
    marginBottom: 15
  },
  text: {
    color: "white"
  }
});
