import * as React from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Alert,
  ScrollView,
  Button
} from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import axios from "axios";

export default class DashBoard extends React.Component {
  constructor(props) {
    super(props);
  }
  static navigationOptions = {
    title: "Projects Detail"
  };

  // fetchData=async () => {
  //   let token = this.props.navigation.state.params.token.token;
  //   const response = await fetch("http://34.67.239.2/phuonglhk_app/public/api/projects", {
  //     headers: { Authorization: `Bearer ${token}` }
  //   });
  //   const json= await response.json();
  //   this.setState({listProject:json.results});
  // }

  render() {
    return (
      <View>
        <TextInput
          style={styles.input}
          onChangeText={text => onChangeText(text)}
          value={this.props.navigation.state.params.project.projectName}
        />
        <TextInput
          style={styles.input}
          onChangeText={text => onChangeText(text)}
          value={this.props.navigation.state.params.project.projectDescription}
        />
        <TextInput
          style={styles.input}
          onChangeText={text => onChangeText(text)}
          value={this.props.navigation.state.params.project.projectOwner}
        />
        <TextInput
          style={styles.input}
          onChangeText={text => onChangeText(text)}
          value={this.props.navigation.state.params.project.startDate}
        />
        <TextInput
          style={styles.input}
          onChangeText={text => onChangeText(text)}
          value={this.props.navigation.state.params.project.endDate}
        />
        <TextInput
          style={styles.input}
          onChangeText={text => onChangeText(text)}
          value={this.props.navigation.state.params.project.projectStatus}
        />
        <Button title="Update" />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  row: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center"
  },
  input: {
    backgroundColor: "#ffffff",
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 10,
    borderWidth: 0.5,
    borderColor: "#d6d7da"
  }
});
