import * as React from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  FlatList,
  ScrollView,
  Button,
} from "react-native";

import { createStackNavigator, createAppContainer } from "react-navigation";
// You can import from local files
import axios from "axios";
import Logo from "../components/Logo";
import { TextInput } from "react-native-gesture-handler";
import Project from "../components/Project";

class LogoTitle extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Image
        source={require("../assets/00f0c817-375b-4710-a67a-dc78eba9e390_200x200.png")}
        style={{ width: 70, height: 70 }}
      />
    );
  }
}

export default class DashBoard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listProject: [],
    };
  }
  goToCreateProject = () => {
    this.props.navigation.navigate("Login");
    // console.log("ok");
  };

  static navigationOptions = {
    title: "List projects",
    headerRight: (
      <Button title="Create New" onPress={_ => this.goToCreateProject} />
    ),
  };

  // fetchData=async () => {
  //   let token = this.props.navigation.state.params.token.token;
  //   const response = await fetch("http://34.67.239.2/phuonglhk_app/public/api/projects", {
  //     headers: { Authorization: `Bearer ${token}` }
  //   });
  //   const json= await response.json();
  //   this.setState({listProject:json.results});
  // }

  fetchData = () => {
    let token = this.props.navigation.state.params.token.token;
    var that = this;
    axios
      .get("http://34.67.239.2/phuonglhk_app/public/api/projects", {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(function(response) {
        // handle success

        that.setState({ listProject: response.data.data });
      });
  };

  componentWillMount() {
    this.fetchData();
  }

  render() {
    return (
      <ScrollView>
        <FlatList
          data={this.state.listProject}
          renderItem={({ item }) => (
            <Project project={item} navigation={this.props.navigation} />
          )}
          keyExtractor={item => `${item.id}`}
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 40,
  },
  title: {},
});
