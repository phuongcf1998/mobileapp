import React from "react";

import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

import Login from "./screens/Login";
import DashBoard from "./screens/DashBoard";
import ProjectDetail from "./screens/ProjectDetail";
import Register from "./screens/Register";
import CreateProject from "./screens/CreateProject";

const AppNavigator = createStackNavigator({
  Login: {
    screen: Login
  },
  DashBoard: {
    screen: DashBoard
  },
  ProjectDetail: {
    screen: ProjectDetail
  },
  Register: {
    screen: Register
  },
  CreateProject : {
    screen : CreateProject
  },
});
export default AppNavigator;
